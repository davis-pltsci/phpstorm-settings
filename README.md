# README #

Platform Science PHPStorm Settings

### What is this repository for? ###

* Sync developer IDE settings across the team

### How do I get set up? ###

* Enter git repo under File > Settings Repository
* Choose Overwrite Local

### Contribution guidelines ###

* Make sure team is on board with settings changes
* Don't force overwrite changes other develoers don't expect

### Who do I talk to? ###

* Web Developer Team